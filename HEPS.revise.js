// ==UserScript==
// @name        HEPS
// @namespace   http://www.dl.kuis.kyoto-u.ac.jp/~manabe/
// @description a HEading-based Page Segmentation algorithm
// @include     *
// @version     1.0.4
// @grant       none
// ==/UserScript==

window.HEPS = top.HEPS || new function () {

    var undefined = void 0,
        ROOT = window.document.body, // Root node of analysis
        EXTRACT_URL = true,
        EXTRACT_PAGE_HEADING = true,
        EXTRACT_TEXT_OF_IMG  = true;

    var scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
    var scrollY = document.documentElement.scrollTop || document.body.scrollTop;

    var scaler = {
            top:            {min: -0.05875, max: 8.044171875},
            left:           {min: 0.0 ,max : 0.999375 },
            width:          {min: 0.000625 ,max : 1.0 },
            height:         {min: 0.00025 ,max : 3.2239999999999998 },
            links:          {min: 0.0 ,max : 79.0 },
            tags:           {min: 0.0 ,max : 283.5 },
            text:           {min: 0.0 ,max : 4838.0 },
            nodes:          {min: 0.0 ,max : 518.0 },
            textPunc:       {min: 0.0 ,max : 0.9876543209876544 },
            textNum:        {min: 0.0 ,max : 0.875 },
            bBColor:        {min: 0.0 ,max : 1.0 },
            bLColor:        {min: 0.0 ,max : 1.0 },
            bRColor:        {min: 0.0 ,max : 1.0 },
            bTColor:        {min: 0.0 ,max : 1.0 },
            color:          {min: 0.0 ,max : 1.0 },
            fontSize:       {min: 8.0 ,max : 60.0 },
            fontWeight:     {min: 300.0 ,max : 900.0 },
    };

    function MyArray(array) {

        this.push.apply(this, array || []);

        return this;

    }

    MyArray.getGetter = function(key) {

        return function(object) {

            return object[key];

        };

    };

    MyArray.prototype = (function() {

        this.constructor = MyArray;

        this.calcRatio = function(func) {

            var cnt = 0;

            this.forEach(function(replica, i, replicaArray) {

                if(func(replica, i, replicaArray) ) cnt += 1;

            });

            return (cnt / this.length);

        };

        this.concat = function(that) {

            return new MyArray(this.toArray().concat(that.toArray() ) );

        };

        this.getLast = function(i) {

            return this[this.length - 1 + (i || 0)];

        };

        this.map = function() {

            return new MyArray(Array.prototype.map.apply(this, arguments) );

        };

        this.setEvery = function(key, value) {

            this.forEach(function(object) {

                object[key] = value;

            });

        };

        this.toRange = function() {

            return({

                "from": this[0].from,
                "to": this.getLast().to,
                "mandatory": true,

            });

        };

        this.toArray = function() {

            return this.slice(0);

        };

        this.toString = function() {

            return this.toArray().toString();

        }

        this.getFrontNodes = function() { // Section 4.3.1

            var targetArray = this,
                resultArray,
                getParent = MyArray.getGetter("parent");

            function isNotNext(replica, i, replicaArray) {

                return replica !== replicaArray[i + 1];

            }

            if(this.frontNodes) return this.frontNodes; // Get cache

            switch(this.length) {

                case 0:

                    resultArray = new MyArray([]);
                    break;

                case 1:

                    resultArray = new MyArray([this[0].root]);
                    break;

                default:

                    while(targetArray.every(isNotNext) ) {

                        resultArray = targetArray;
                        targetArray = targetArray.map(getParent);

                    }

            }

            this.forEach(function(replica, i) {

                replica.frontNode = resultArray[i];

            });

            this.frontNodes = resultArray; // Set cache

            return resultArray;

        };

        this.calcNodeArrays = function() { // Section 4.4.1

            return this.getFrontNodes().map(function(frontNode, i, frontNodes) {

                var targetNode    = frontNode.next,
                    nextFrontNode = frontNodes[i + 1],
                    currentBlock  = frontNode.getContextBlock(),
                    nodeArray     = new MyArray([frontNode]);

                while(!(!targetNode ||              // No following sibling
                    targetNode === nextFrontNode || // Another front node
                    targetNode.hasHeading ||        // Node including headings
                    !currentBlock.includes(targetNode) ) ) {
                    // Node not included in current upper block

                    nodeArray.push(targetNode);
                    targetNode = targetNode.next;

                }

                frontNode.nodeArray = nodeArray;

                return nodeArray;

            });

        };

        return this;

    }).apply(Object.create(Array.prototype) );

    this.MyArray = MyArray;

    function Replica(node, parentReplica) {

        function chain(elder, younger) {

            elder.next = younger;
            younger.prev = elder;

        }

        function extractPageHeading(node) {

            var document  = node.ownerDocument,
                titleNode = document.getElementsByTagName("title")[0],
                baseNode  = document.getElementsByTagName("base")[0],
                title,
                url,
                locationHref = document.location.href;

            if(titleNode) {
                // 如果包含标题
                return normalizeSpace(titleNode.textContent);

            } else {

                if(baseNode && baseNode.href) {

                    url = baseNode.href;

                    if(/\/$/.test(url) )
                        url += new MyArray(locationHref.split("/") ).getLast();

                } else {

                    url = locationHref;

                }

                url = tokenizeURL(url);

                return normalizeSpace(url);

            }

        }

        function isDisplay(node, name){
            var getCS  = node.ownerDocument.defaultView.getComputedStyle,
                target = (name === "#text") ? node.parentNode : node;
            var style = getCS(target, null);
            var rect = target.getBoundingClientRect();

            return  rect.width !== 0 &&
                    rect.height !== 0 &&
                    style.display !=='none' &&
                    style.visibility !== 'hidden';
        }

        function getStyle(node, name, context) {
            // 获取样式
            var getCS  = node.ownerDocument.defaultView.getComputedStyle,
                target = (name === "#text") ? node.parentNode : node,
                styObj = getCS(target, null);

            function parseWeight(value) {

                switch (value) {

                    case "normal": return 400;

                    case "bold":   return 700;

                    default:       return parseInt(value);

                }

            }

            return {

                color:          styObj.color,
                fontSize:       parseFloat(styObj.fontSize),
                fontStyle:      styObj.fontStyle,
                fontWeight:     parseWeight(styObj.fontWeight),
                offsetHeight:   (name === "img") ? node.offsetHeight : "null",
                tagPath:        context + "/" + name,
                textDecoration: styObj.textDecoration,

            };

        }

        function normalizeSpace(string) {

            return string.replace(/\s+/g, " ").replace(/^ | $/g, "");

        }

        function tokenizeURL(url) {

            url = new MyArray(url.split("://") ).getLast();

            return url.split(/\W+/).join(Replica.RAWSTRING_SEP);

        }

        // 持有对node的引用
        this.node = node;
        this.name = node.nodeName.toLowerCase();
        this.visible = true;

        if(parentReplica) {

            this.parent = parentReplica;
            this.ancestors = new MyArray([this]).concat(this.parent.ancestors);
            this.style = getStyle(node, this.name, this.parent.style.tagPath);

        } else { // this is root

            if(EXTRACT_PAGE_HEADING) {

                this.pageHeadingRange = {"from": 0, "mandatory": true};
                this.rawString = extractPageHeading(node);
                this.pageHeadingRange.to = this.rawString.length;

            } else {

                this.rawString = "";

            }

            this.id2replica = [];
            this.ancestors = new MyArray([this]);
            this.style = getStyle(node, this.name, "");

        }

        this.root = this.ancestors.getLast();
        this.depth = this.ancestors.length;
        this.id = this.root.id2replica.length; // ID number (== document order)
        this.root.id2replica.push(this);

        if (this.name === "#text") {

            this.isText = true;
            this.content = normalizeSpace(node.textContent);
            if (!isDisplay(node, this.name)){
                this.visible = false;
                return;
            }

        } else if (this.name === "img") {
            if (!isDisplay(node, this.name)){
                this.visible = false;
                return;
            }
            
            if(EXTRACT_TEXT_OF_IMG) {

                this.content = normalizeSpace(
                    (node["src"] ? tokenizeURL(node.getAttribute("src")) : "") +
                    Replica.RAWSTRING_SEP + node["alt"] || "");

            } else {

                this.content = "<IMG:" +
                    encodeURIComponent(node.getAttribute("src") || Replica.NOSRC) +
                    ">";

            }

        }

        this.from = this.root.rawString.length + Replica.RAWSTRING_SEP.length;
        // Content offset in rawString
        this.hasIDfrom = this.id;

        if(this.content){
            this.root.rawString += (Replica.RAWSTRING_SEP + this.content);
            // 提取feature
            this.feature_obj = extract_feature(node);
        }

        
        // 检查display属性是否为none，若是则不需要再递归建replica
        // if (!isDisplay(node, this.name)){
        //     this.visible = false;
        //     return;
        // }

        
        // 自顶向下
        if(!Replica.IGNORE_CHILDREN_OF[this.name]) {

            [].forEach.call(node.childNodes, function(childNode) {
                if(-1 < Replica.SCAN_CHILDREN_OF.indexOf(childNode.nodeType) ) {
                    var replica_child = new Replica(childNode, this);
                    if (replica_child.visible == true){
                        this.push(replica_child);

                        if(1 < this.length) chain(this.getLast(-1), this.getLast() );
                    }
                    

                }

            }, this);

        }

        this.to = this.root.rawString.length; // Content limit in rawString
        this.hasIDto = this.getLast() ? this.getLast().hasIDto : this.id;

        return this;

    }

    Replica.IGNORE_CHILDREN_OF = { // Descendants of these nodes will be ignored

        iframe:   true,
        noscript: true,
        script:   true,
        style:    true,

    };

    Replica.RAWSTRING_SEP = " ";

    Replica.NOSRC = "no-src";

    Replica.SCAN_CHILDREN_OF = [Node.ELEMENT_NODE, Node.TEXT_NODE];
    // Scan these types of nodes

    Replica.prototype = (function() {

        this.constructor = Replica;

        this.breaksSentence = function() {

            return (this.next && this.next.isText) ||
                (this.prev && this.prev.isText);

        };

        this.getContextBlock = function() {

            var i = 0,
                ancestors = this.ancestors,
                ancestorReplica;

            for(; i < ancestors.length; i++) {

                ancestorReplica = ancestors[i];

                if(ancestorReplica.isPartOf) return ancestorReplica.isPartOf;

            }

            return undefined;

        };

        this.includes = function(that) {

            return (this.hasIDfrom <= that.id) && (that.id <= this.hasIDto);

        };

        this.isBlank = function() {

            return (this.isText && !this.content);

        };

        this.merge = function(that, rawString) { // Merge two text replicas

            this.from      = this.from || that.from;
            this.to        = that.to || this.to;
            this.content   = rawString.substring(this.from, this.to);
            this.hasIDfrom = this.hasIDfrom || that.hasIDfrom;
            this.hasIDto   = that.hasIDto || this.hasIDto;
            
            // 合并 feature
            this.feature_obj = merge_feature(this, that);
            that.parent.removeChild(that);

            return(this);

        };

        this.removeChild = function(childReplica) {

            this.splice(this.indexOf(childReplica), 1);
            childReplica.parent    = undefined;
            childReplica.ancestors = new MyArray([childReplica]);

            if(childReplica.prev) childReplica.prev.next = childReplica.next;

            if(childReplica.next) childReplica.next.prev = childReplica.prev;

            return childReplica;

        };

        this.stringifyStyle = function() {

            return Object.keys(this.style).sort().map(function(key) {

                return this.style[key];

            }, this).join(";");

        };

        this.toText = function(rawString) {

            var i = this.length - 1;

            this.name    = "#text";
            this.isText  = true;
            this.content = rawString.substring(this.from, this.to);
            
            this.feature_obj = this[0] ? this[0].feature_obj : null;
        
            for (; 0 < i; i--){
                this.feature_obj = merge_feature(this, this[i]);
            }

            i = this.length - 1;
            for (; 0 <= i; i--){
                this.removeChild(this[i]);
            }


            
            return this;

        };

        this.traverse = function(func) {
            // DFS搜索
            var iterate_children = func(this),
                i;

            if(iterate_children) {

                for(i = this.length - 1; 0 <= i; i--) this[i].traverse(func);

            }

        };

        return this;

    }).apply(Object.create(MyArray.prototype) );

    this.Replica = Replica;

    function Block(nodeArray, heading) {

        var content = nodeArray.toRange(),
            contextBlock = nodeArray[0].getContextBlock();

        this.nodeArray = nodeArray;
        this.nodes = new Array();
        nodeArray.forEach(function(val){this.nodes.push(val.node)}, this);
        this.x1 = Number.MAX_VALUE;
        this.y1 = Number.MAX_VALUE;
        this.x2 = 0;
        this.y2 = 0;

        

        this.nodes.forEach(function(node){
            var rect = node.getBoundingClientRect ? node.getBoundingClientRect() : node.parentElement.getBoundingClientRect();    
            var x = rect.x + scrollX;
            var y = rect.y + scrollY;
            if (x < this.x1){
                this.x1 = x;
            }
            if (y < this.y1){
                this.y1 = y;
            }
            if (x + rect.width > this.x2){
                this.x2 = x + rect.width;
            }
            if (y + rect.height > this.y2){
                this.y2 = y + rect.height;
            }
        }, this);




        nodeArray.setEvery("isPartOf", this);
        this.contents = [content];
        this.children = [];

        if(heading) {
            this.style    = heading.stringifyStyle();
            //this.headingReplica = heading;
            this.heading  = new MyArray([heading]).toRange();
            this.headings = [ [this.heading] ];
            heading.ancestors.setEvery("hasHeading", true);

        }

        if(contextBlock) contextBlock.children.push(this);

        return this;

    }

    Block.prototype = (function() {

        this.constructor = Block;

        this.includes = function(replica) {

            return this.nodeArray.some(function(part) {

                return part.includes(replica);

            });

        };

        return this;

    }).apply(Object.create(Object.prototype) );

    function merge_feature(obj1, obj2){
        if (obj1.feature_obj == null || obj2.feature_obj == null){
            return obj1.feature_obj == null ? obj2.feature_obj : obj1.feature_obj;
        }

        obj1.feature_obj.width  += obj2.feature_obj.width;
        obj1.feature_obj.height += obj2.feature_obj.height;
        obj1.feature_obj.left   += obj2.feature_obj.left;
        obj1.feature_obj.top    += obj2.feature_obj.top;
        obj1.feature_obj.bBColor = (obj1.feature_obj.bBColor + obj2.feature_obj.bBColor) / 2;
        obj1.feature_obj.bLColor = (obj1.feature_obj.bLColor + obj2.feature_obj.bLColor) / 2;
        obj1.feature_obj.bRColor = (obj1.feature_obj.bRColor + obj2.feature_obj.bRColor) / 2;
        obj1.feature_obj.bTColor = (obj1.feature_obj.bTColor + obj2.feature_obj.bTColor) / 2;
        obj1.feature_obj.color   = (obj1.feature_obj.color + obj2.feature_obj.color) / 2;
        obj1.feature_obj.fontSize   = (obj1.feature_obj.fontSize + obj2.feature_obj.fontSize) / 2;
        obj1.feature_obj.fontWeight = (obj1.feature_obj.fontWeight + obj2.feature_obj.fontWeight) / 2;
        obj1.feature_obj.text       += obj2.feature_obj.text;
        obj1.feature_obj.textPunc   += obj2.feature_obj.textPunc;
        obj1.feature_obj.textNum    += obj2.feature_obj.textNum;

        return obj1.feature_obj;
    }

    function extract_feature(domNode){

        var obj, parent, text, src;
        var element = domNode;

        if (element.nodeName === '#text'){
            parent = element.parentElement;
            text = element.nodeValue.trim();
            src = "";
        }else if (element.nodeName === 'IMG'){
            parent = element;
            text = element.alt;
            src = encodeURIComponent(element.getAttribute("src") || "no-src");
        }

        var rect = parent.getBoundingClientRect();

        var getCS = document.defaultView.getComputedStyle;
        var styleObj = getCS(parent);

        function parseWeight(value){
            switch (value) {

                case "normal": return 400;

                case "bold":   return 700;

                default:       return parseInt(value);
            }
        }

        function cal_text_stat(text){
            var chars = text.split(/\s+/).join('');

            var punc = ['.', ',', '!', '-', ':', '(', ')',
            '。', '，', '！', '——', '：', '（', '）'];

            var num = ['0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9'];


            var length = 0;
            var punc_n = 0;
            var num_n  = 0;

           
            for (var i=0; i < chars.length; i++){
                length += chars[i].length;
                if (punc.includes(chars[i])){
                    punc_n += 1;
                }
                if (num.includes(chars[i])){
                    num_n += 1;
                }
            }
            

            return {
                char:   length,
                punc:   punc_n,
                num:    num_n
            }
        }

        function cal_color_code(color){
            var idx = color.indexOf('(');
            code = color.substring(idx+1, color.length-1);
            rgb_code = code.split(',');

            var tot = 0;
            for (var i=0; i < rgb_code.length; i++){
                tot += parseInt(rgb_code[i]);
            }

            return tot / (3*255);
        }

        var text_stat = cal_text_stat(text);

        obj = {
            nodeType:       element.nodeName == '#text' ? 0 : 1,
            top:            (rect.top + scrollY) / 4000,
            left:           (rect.left + scrollX) / 1600,
            width:          (rect.width) / 1600,
            height:         (rect.height) / 4000,
            links:          parent.getElementsByTagName('a').length,
            tags:           parent.childElementCount,
            text:           text_stat.char,
            nodes:          parent.childNodes.length,
            textPunc:       text_stat.punc,
            textNum:        text_stat.num,
            bBColor:        cal_color_code(styleObj.borderBottomColor),
            bLColor:        cal_color_code(styleObj.borderLeftColor),
            bRColor:        cal_color_code(styleObj.borderRightColor),
            bTColor:        cal_color_code(styleObj.borderRightColor),
            color:          cal_color_code(styleObj.color),
            fontSize:       parseFloat(styleObj.fontSize),
            fontWeight:     parseWeight(styleObj.fontWeight),
        }

        return obj;
    }

    function scale_feature(rootReplica){
        // 第二次scale对应值
        var X = [];
        var pos2Replica = [];

        rootReplica.traverse(function(replica){
            if (replica.feature_obj){
                var obj = replica.feature_obj;
                obj.top   = (obj.top - scaler.top.min) / (scaler.top.max - scaler.top.min);
                obj.left  = (obj.left - scaler.left.min) / (scaler.left.max - scaler.left.min);
                obj.width = (obj.width - scaler.width.min) / (scaler.width.max - scaler.width.min);
                obj.height= (obj.height - scaler.height.min) / (scaler.height.max - scaler.height.min);
                obj.links = (obj.links - scaler.links.min) / (scaler.links.max - scaler.links.min);
                obj.tags  = (obj.tags - scaler.tags.min)  / (scaler.tags.max - scaler.tags.min);
                obj.text  = (obj.text - scaler.text.min) / (scaler.text.max - scaler.text.min);
                obj.nodes = (obj.nodes - scaler.nodes.min) / (scaler.nodes.max - scaler.nodes.min);
                obj.textPunc = (obj.textPunc - scaler.textPunc.min) / (scaler.textPunc.max - scaler.textPunc.min);
                obj.textNum  = (obj.textNum - scaler.textNum.min) / (scaler.textNum.max - scaler.textNum.min);
                obj.bBColor  = (obj.bBColor - scaler.bBColor.min) / (scaler.bBColor.max-scaler.bBColor.min)
                obj.bLColor  = (obj.bLColor - scaler.bLColor.min) / (scaler.bLColor.max-scaler.bLColor.min);
                obj.bRColor  = (obj.bRColor - scaler.bRColor.min) / (scaler.bRColor.max-scaler.bRColor.min);
                obj.bTColor  = (obj.bTColor - scaler.bTColor.min) / (scaler.bTColor.max-scaler.bTColor.min);
                obj.color    = (obj.color - scaler.color.min) / (scaler.color.max-scaler.color.min);
                obj.fontSize = (obj.fontSize - scaler.fontSize.min) / (scaler.fontSize.max-scaler.fontSize.min);
                obj.fontWeight=(obj.fontWeight - scaler.fontWeight.min) / (scaler.fontWeight.max-scaler.fontWeight.min);

                var rowArray = Object.keys(obj).map(function(key){return obj[key];});
                X.push(rowArray);
                pos2Replica.push(replica);
            }

            return true;
        });

        return {
            'X' : X,
            'pos2Replica': pos2Replica
        }


    }

    function preprocess(rootReplica, rawString) { // Section 4.1

        var breakingStyle = {};

        rootReplica.traverse(function(replica) {

            if(replica.isBlank() ) {

                replica.parent.removeChild(replica);

                return false;

            } else return true;

        });

        rootReplica.traverse(function(replica) {

            if(replica.breaksSentence() ) {

                breakingStyle[replica.stringifyStyle()] = replica.next ? replica.next.style : replica.prev.style;

            }

            return true;

        });

        rootReplica.traverse(function(replica) {

            var newStyle = breakingStyle[replica.stringifyStyle()],
                current;

            if(newStyle) {

                current = replica;
                current.toText(rawString);

                if(current.prev && current.prev.isText){
                    current = current.prev.merge(current, rawString);

                }

                if(current.next && current.next.isText)
                    current.merge(current.next, rawString);

                current.style = newStyle;

                return false;

            } else return true;

        });

    }

    function classify_title(testObj, callback){
        const Http = new XMLHttpRequest();
        const url = "http://localhost:8000/segs/set_title/";
        Http.open("POST", url);
        Http.setRequestHeader('content-type', 'application/json');

        Http.onreadystatechange = function(){
            if (this.readyState == 4 && this.status == 200){
                var ret = JSON.parse(Http.responseText);
                var result = ret.result;

                for (var i=0; i < result.length; i++){
                    var replica = testObj.pos2Replica[i];
                    if (result[i] == 1){
                        replica.isTitle = true;
                    }else{
                        replica.isTitle = false;
                    }
                }

                callback(testObj, result);
            }
        }

        var data = {'data' : JSON.stringify(testObj.X)}
        Http.send(JSON.stringify(data));
    }

    function classify(rootReplica) { // Section 4.2
        // 将相同样式的节点放在一个array里
        var styleHash = {};

        rootReplica.traverse(function(replica) {

            var style;

            if(replica.content) {

                style = replica.stringifyStyle();
                styleHash[style] = styleHash[style] || new MyArray();
                styleHash[style].unshift(replica);

            }

            return true;

        });

        return Object.keys(styleHash).map(function(key) {

            return styleHash[key];

        });

    }

    function sort(nodeLists) { // Section 4.3

        return nodeLists.sort(function(set0, set1) {

            var replica0   = set0[0],
                frontNode0 = set0.getFrontNodes()[0],
                replica1   = set1[0],
                frontNode1 = set1.getFrontNodes()[0];

            return (frontNode0.depth - frontNode1.depth) || // Block depth
                (replica1.style.fontSize - replica0.style.fontSize) ||
                (replica1.style.fontWeight - replica0.style.fontWeight) ||
                (replica0.id - replica1.id); // Document order

        });

    }

    function construct(rootReplica, sortedNodeLists) { // Section 4.4

        var rootBlock = new Block(new MyArray([rootReplica]) );

        function isEmpty(replica) {

            var content = replica.frontNode.nodeArray.toRange();

            return (replica.from <= content.from) &&
                (content.to <= replica.to);

        }

        function hasNoSiblingCandidate(replica0, dummy, nodeList) {

            var context = replica0.getContextBlock();

            return nodeList.every(function(replica1) {

                return !( (replica0 !== replica1) &&
                    (context === replica1.getContextBlock() ) );

            });

        }

        function isNonUnique(replica0, dummy, nodeList) {

            var content = replica0.content,
                context = replica0.getContextBlock();

            return nodeList.some(function(replica1) {

                return ( (replica0 !== replica1) &&
                   (content === replica1.content) &&
                   (context === replica1.getContextBlock() ) );

            });

        }

        function isTooMuch(t) {

            return(function(replica) {

                var content = replica.frontNode.nodeArray.toRange();

                return (content.to - content.from) <
                    t * (replica.to - replica.from);

            });

        }

        sortedNodeLists.forEach(function(nodeList) {

            var frontNodes = nodeList.getFrontNodes();

            frontNodes.calcNodeArrays();

            // Set-level filtering
            // nodeList.isFilteredBy = "";

           /*  if(0.1 <= frontNodes.calcRatio(MyArray.getGetter("hasHeading") ) )
                nodeList.isFilteredBy += "Including upper-level headings;";

            if(0.5 <= nodeList.calcRatio(isEmpty) )
                nodeList.isFilteredBy += "Producing empty block;";

            if(0.9 <= nodeList.calcRatio(hasNoSiblingCandidate) )
                nodeList.isFilteredBy += "No sibling candidates;";

            if(0.4 <= nodeList.calcRatio(isNonUnique) )
                nodeList.isFilteredBy += "Non-unique contents;";

            if(0.3 <= nodeList.calcRatio(isTooMuch(1.5) ) )
                nodeList.isFilteredBy += "Too much content as a heading;";

            if(nodeList.isFilteredBy) return; */

            nodeList.filter(function(replica) { // Node-level filtering
                // 非空且 hasHeading = false
                return(!(replica.frontNode.hasHeading || isEmpty(replica) ) );

            }).forEach(function(replica) {
                // 更改heading颜色
                //replica.frontNode.node.style.backgroundColor = "yellow";
                //replica.frontNode.node.style.color = "red";
                replica.parent.node.style.color = "red";
                new Block(replica.frontNode.nodeArray, replica);

            });

        });

        return rootBlock;

    }

    function before(context){
        context.rootReplica = new Replica(ROOT); // Construct Virtual DOM
        preprocess(context.rootReplica, context.rootReplica.rawString);
        context.trainObj = scale_feature(context.rootReplica);

        classify_title(context.trainObj, function(testObj, result){
            var pos2Replica = testObj.pos2Replica;
            for (var i=0; i < pos2Replica.length; i++){
                var replica = pos2Replica[i];
                if (replica.isTitle){
                    console.log(replica.content, replica.isTitle);
                }
            }
        });
    }

    function main(context){
        context.nodeLists   = classify(context.rootReplica);

        context.nodeLists   = sort(context.nodeLists);
        context.rootBlock   = construct(context.rootReplica, context.nodeLists);

        if(context.rootReplica.pageHeadingRange)
            context.rootBlock.headings = [ [context.rootReplica.pageHeadingRange] ];

        context.rootBlock.rawString = context.rootReplica.rawString;

        if(EXTRACT_URL) {
            context.rootBlock.URL = ROOT.ownerDocument.location.href;
            context.rootBlock.baseURL = ROOT.baseURI;
        }

        // flat嵌套block, 以标准形式
        function assembleSegments(block, segmentations){
            var multiPolygonSet = new Array();
            var multiPolygon = new Array();
            var polygon = new Array();
            var start = new Array(Math.round(block.x1), Math.round(block.y1));
            polygon.push(start);
            polygon.push(new Array(Math.round(block.x1), Math.round(block.y2)));
            polygon.push(new Array(Math.round(block.x2), Math.round(block.y2)));
            polygon.push(new Array(Math.round(block.x2),  Math.round(block.y1)));
            polygon.push(start);
            multiPolygon.push(polygon);
            multiPolygonSet.push(multiPolygon);
            segmentations.push(multiPolygonSet);

            for (var _i=0, _a = block.children; _i < _a.length; _i++){
                var child = _a[_i];
                assembleSegments(child, segmentations);
            }
        }

        var segmentations = new Array();
        assembleSegments(context.rootBlock, segmentations);

        context.json = JSON.stringify({"id": "TBFWID", "height": window.innerHeight, "width": window.innerWidth, "segmentations": {"heps": segmentations}});


        if(typeof window.testHEPS == "function") {
            context.test_status = window.testHEPS(context);
        } else {
            console.log("HEPS: Complete.");
        }
    }

    // 用来debug，定位block
    function debug(context){
        function assembleBlocks(block, block_arr){
            if (block.nodes.length > 0 && block.nodes[0].nodeName != "BODY"){
                block_arr.push(block);
            }
            
            for (var _i=0, _a = block.children; _i < _a.length; _i++){
                var child = _a[_i];
                assembleBlocks(child, block_arr);
            }
            
        }
    
        context.blocks = new Array();
        assembleBlocks(this.rootBlock, this.blocks);
    
        // 检查两个block是否相交，如果相交，返回相交面积
        context.rectint = function(blockA, blockB){
            //矩形A左下角, 右上角
            var xa1 = blockA.x1, ya1 = blockA.y2;
            var xa2 = blockA.x2, ya2 = blockA.y1;
            // B的左下角，右上角
            var xb1 = blockB.x1, yb1 = blockB.y2;
            var xb2 = blockB.x2, yb2 = blockB.y1;
    
            // 两矩形中心点
            var ox = (xa1+xa2)/2, oy = (ya1+ya2)/2;
            var ex = (xb1+xb2)/2, ey = (yb1+yb2)/2;
    
            // 注意浏览器的坐标轴，向右，向下是正值
            var intersected = ((Math.abs(ex-ox) <= ((xa2-xa1)/2 + (xb2-xb1)/2))
               && (Math.abs(ey-oy) <= ((ya1-ya2)/2 + (yb1-yb2)/2)))
    
            if (!intersected){
                return 0;
            }else{
                var cx = Math.max(xa1, xb1), cy = Math.min(ya1, yb1);
                var fx = Math.min(xa2, xb2), fy = Math.max(ya2, yb2);
                return Math.abs(cx-fx) * Math.abs(cy-fy);
            }
        }
    
        // 计算哪个block与给定的矩形相交面积最大
        context.locateRefBlock = function(rect){
            var _rects = new Array();
    
            for (var _i=0; _i < context.blocks.length; _i++){
                var b = context.blocks[_i];
                var pair = new Array();
                pair.push(_i);
                pair.push(context.rectint(b, rect));
                _rects.push(pair);
            }
    
            _rects.sort(function(set0, set1){
                var score0 = set0[1];
                var score1 = set1[1];
                return score1-score0;
            });
    
            _rects.forEach(function(ele){
                console.log(ele[0] + ' ' + ele[1]);
            });
    
            return this.blocks[_rects[0][0]];
        }
    }

    function visualize(context){
        // 画图设置
        function initContext(){
            var root = document.body;
            var rootBox = root.getBoundingClientRect();
        
            // 添加画布
            var canvas = document.createElement('canvas'); 
            canvas.width = rootBox.width;
            canvas.height = rootBox.height;
            // 画布位置
            canvas.style.position='absolute';
            canvas.style.left=0;
            canvas.style.top=0;
            canvas.style.zIndex=100000;
            canvas.style.pointerEvents='none'; //Make sure you can click 'through' the canvas
            document.body.appendChild(canvas); //Append canvas to body element
            var context = canvas.getContext('2d');
        
            // 虚线
            context.setLineDash([10]);
            context.lineWidth = "1";
            context.strokeStyle = "black";
            return context;
        }

        function drawSegment(seg_json, context){
            var seg = JSON.parse(seg_json);
            var blocks = seg.segmentations.heps;
        
            for (var i=0; i < blocks.length; i++){
                try{
                    var ele = blocks[i][0][0];
                    var x = ele[0][0], y = ele[0][1];
                    var width = ele[2][0] - x;
                    var height = ele[2][1] - y;
                    //console.log(x, y, width, height);
                    // 修正宽度
                    context.strokeRect(x, y, width*0.99, height*0.995);
                }catch (error){
                    console.error(error);
                }
            }
        }


        context.context = initContext();
        drawSegment(context.json, context.context);

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
      
    // 画图工具方法
        context.drawRect = function(rect, context){
            var width = rect.x2 - rect.x1;
            var height = rect.y2 - rect.y1;
            context.fillStyle = getRandomColor();
            context.fillRect(rect.x1, rect.y1, width, height);
        }
    }

    before(this);
    main(this);
    visualize(this);
    
};
