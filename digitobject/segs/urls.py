from django.urls import path

from . import views

urlpatterns = [
    path('set_title/', views.set_title, name='set_title')
]