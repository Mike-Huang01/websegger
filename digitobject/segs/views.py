import os
import pickle
import json

import numpy as np

from django.shortcuts import render
from django.http import JsonResponse
from django.conf import settings
from django.views.decorators.http import require_http_methods
# Create your views here.

@require_http_methods(['POST'])
def set_title(request):

    json_data = json.loads(request.body)

    try:
        data_ser = json_data['data']
        data = eval(data_ser)
    except KeyError:
        return JsonResponse({'message' : 'Malformed data', 'result': []}, status=400)

    with open(os.path.join(settings.MODEL_ROOT, 'title_model.sav'), 'rb') as f:
        clf = pickle.load(f)
        result = clf.predict(data)
    
    return JsonResponse({'message' : 'ok', 'result': result.tolist()}, status=200)